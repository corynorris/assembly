/*
 * Palm.h
 *
 * header file for Palm
 *
 * This wizard-generated code is based on code adapted from the
 * stationery files distributed as part of the Palm OS SDK 4.0.
 *
 * Copyright (c) 1999-2000 Palm, Inc. or its subsidiaries.
 * All rights reserved.
 */
 
#ifndef PALM_H_
#define PALM_H_

/*********************************************************************
 * Internal Structures
 *********************************************************************/

typedef struct PalmPreferenceType
{
	UInt8 replaceme;
} PalmPreferenceType;

/*********************************************************************
 * Global variables
 *********************************************************************/

extern PalmPreferenceType g_prefs;

/*********************************************************************
 * Internal Constants
 *********************************************************************/

#define appFileCreator			'STRT'
#define appName					"Palm"
#define appVersionNum			0x01
#define appPrefID				0x00
#define appPrefVersionNum		0x01

asm intro();	// added to define the prototype
				// required for this lab .. NZ

#endif /* PALM_H_ */
