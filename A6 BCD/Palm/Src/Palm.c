/* File: Palm.c
NAME:	Cory Norris
STUDENTID:	080704410
EMAIL:	norr4410@wlu.ca

enter name, student ID#, and email address here
*/

#include <PalmOS.h>
#include "cp216Lib.h"
#include "Palm.h"

asm intro()
{
	JSR		ClearScreen

	// change window title
	LEA		ShTitle,A0		// load window title
	JSR		SetTitle		// initialize window
	
		
	// write string to screen
	LEA		LastName,A0		// load string pointer
	JSR		WriteString		// write string
	
	//write student id
	LEA		ID,A0			//load id pointer
	JSR		WriteString		//write string
	
	LEA		Line,A0			//load an empty line
	JSR		WriteString		//skip line
	JSR		WriteString		//skip line
	MOVE.L	#5,D0
LOOP: 
	JSR		WriteNum		
	LEA		Comma,A0
	CMP		#1,D0
	BEQ		NEXT	
	JSR		WriteString
	SUB.L	#1,D0
	BRA		LOOP
NEXT:	

	LEA		Line,A0		//load an empty line
	JSR		WriteString	//skip to line 8
	JSR		WriteString
	JSR		WriteString
	
	//Ask them to tap when finished
	LEA		AskTap,A0			//load id pointer
	JSR		WriteString			//write string

	
	// wait for user to press a key to continue
	JSR		ReadChar		// wait for keystroke
	JSR		ClearScreen
	
	//Ask them to tap when finished
	LEA		AskNum,A0			//load id pointer
	JSR		WriteString			//write string
	JSR		ReadChar
	JSR		WriteChar			//echo the char
	
	//write the char +1, +2 and +3
	JSR		NewLine
	ADD		#1,D0
	JSR		WriteChar
	
	JSR		NewLine
	ADD		#1,D0
	JSR		WriteChar
	
	JSR		NewLine
	ADD		#1,D0
	JSR		WriteChar
	
	
	//move to line 8
	JSR		NewLine
	JSR		NewLine
	
	//Ask them to tap when finished
	LEA		AskTap,A0			//load id pointer
	JSR		WriteString			//write string
	JSR		ReadChar		// wait for keystroke
	
	//display a message that it's the end of the program	
	LEA		EndAsmt,A0
	JSR		WriteString
	
	RTS


ShTitle:	DC.B	"1st Palm Assignment"
LastName:	DC.B	"Norris\n"
ID:			DC.B	"080704410\n"
Line:		DC.B	"\n"
AskTap:		DC.B	"Please tap the screen when done\n"
AskNum:		DC.B	"Enter a number between 0 and 5\n"
Comma:		DC.B	", "
EndAsmt:	DC.B	"End of 1st Palm Assignment\n"
}