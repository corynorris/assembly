/* File: Palm.c

enter name, student ID#, and email address here
*/

#include <PalmOS.h>
#include "cp216Lib.h"
#include "Palm.h"

asm intro()
{
	// change window title
	LEA		ShTitle,A0		// load window title
	JSR		SetTitle		// initialize window
	
	// write character to screen
	CLR.L	D0				// clear and
	MOVE.B	#'5',D0			// set character
	JSR		WriteChar		// write character
	JSR		NewLine			// go to the next line
	
	// write string to screen
	LEA		IntroStr,A0		// load string pointer
	JSR		WriteString		// write string
	
	// write result of sum to screen
	MOVE.L	#500000,D0		// initialize D0
	MOVE.L	D0,D1			// .. and D1
	ADD.L	D1,D0			// add them
	JSR		WriteNum		// write result
	JSR		NewLine			// go to the next line
	
	// calculate another sum and output
	CLR.L	D0				// clear sum
	MOVE.L	#6,D1			// initialize counter
loop:	
	ADD.L	D1,D0			// add counter to sum
	SUB.L	#1,D1			// decrement counter
	BNE		loop			// loop til zero
	
	JSR		WriteNum		// write result
	
	// wait for user to press a key to continue
	JSR		ReadChar		// wait for keystroke
	
	RTS

ShTitle:	DC.B	"Lab 7: My first Palm program"
IntroStr:	DC.B	" ... and the answer is "
}