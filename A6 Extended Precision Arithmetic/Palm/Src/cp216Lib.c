/* File: cp216Lib.c   ================   This is the implementation of cp216Lib.c.  See the .h file for details.      original version by Douglas Anderson   Spring 1999      Modification History:     03/25/99 - Initial revision.     02/13/02 - PalmOS version 3+ revisions     2007-02-08 - modified by David Brown*/// ----------------------------------// Interfaces...// ----------------------------------#include <PalmOS.h>#include "PalmRsc.h"#include "cp216Lib.h"// ----------------------------------// External globals...// ----------------------------------// Added by N. Z./* This is used to jump back out of the program if we need to quit   while waiting for a character or a mouse-click. */extern ErrJumpBuf gExitJmpbuf;// ----------------------------------// Local prototypes...// ----------------------------------static asm GetConsole();static char GetNextChar();static void WaitNextMouse();// ----------------------------------// Exported functions...// ----------------------------------/* Function: SetTitle()   --------------------   This function sets the title of the current form to the string indicated by A0.   Like most of the other routines here, this is really just a wrapper function   for some OS calls.      Does not trash any registers.  In order to accomplish this, we must save A0-A1   and D0-D2, since these are caller-saved.  A5 is restored from the saved copy, so   that must also be saved.      Input:     A0.L - Input string.  Should point to first character in null-terminated string.            NOTE: the string passed in is NOT copied--it is actually stored by the            OS for future use.  Strings that have been allocated with DC are fine            to use, since those are never freed and shouldn't ever change.   Internal:     A0.L - Initially holds input (goes to A2); holds form pointer temporarily.     A2.L - Holds input string. */asm SetTitle(){	MOVEM.L		A0-A2/A5/D0-D2, -(SP)	MOVEA.L		A0, A2		ASM_SYS_TRAP (sysTrapFrmGetActiveForm)	MOVE.L		A2, -(SP)	MOVE.L		A0, -(SP)	ASM_SYS_TRAP (sysTrapFrmSetTitle)	ADDQ.L		#8, SP	MOVEM.L		(SP)+, A0-A2/A5/D0-D2	RTS}		/* Function: WriteChar()   ---------------------   Writes the character stored in the lowest byte of D0 to the console.  Simply   uses FldInsert (after retreiving the console).  Note that the character must be   stored on the stack so we can pass in a string to FldInsert.      Does not trash any registers.  In order to accomplish this, we must save A0-A1   and D0-D2, since these are caller-saved.  A5 is restored from the saved copy, so   that must also be saved.      Input:     D0.B - Input character.  The lowest byte is the byte written.   Internal:     D0.B - Initially holds input character.     A0   - Holds the console between GetConsole and the FldInsert. */asm WriteChar(){		LINK		A6, #-2		MOVEM.L		A0-A1/A5/D0-D2, -(SP)		MOVE.B		D0, -2(A6)		JSR			GetConsole				MOVE.W		#1, -(SP)		PEA			-2(A6)		MOVE.L		A0, -(SP)		ASM_SYS_TRAP (sysTrapFldInsert)		ADDA.L		#10, SP				MOVEM.L		(SP)+, A0-A1/A5/D0-D2		UNLK		A6		RTS}	/* Function: WriteString()   -----------------------   Writes the C string (null terminated string) stored at the location   indicated by A0.  Simply uses FldInsert (after retreiving the console).      Does not trash any registers.  In order to accomplish this, we must save A0-A1   and D0-D2, since these are caller-saved.  A5 is restored from the saved copy, so   that must also be saved.      Input:     A0.L - Input string.  Should point to first character in null-terminated string.   Internal:     D0.L - Scratch--StrLen returns value to here.     D3.L - Used to store input string length between the call to StrLen and FldInsert,            since GetConsole can trash D0.     A0.L - Input string (moved to A2).  Also, holds console field after GetConsole.     A2.L - Used to store the input string. */asm WriteString(){		MOVEM.L		A0-A2/A5/D0-D3, -(SP)		MOVEA.L		A0, A2			MOVE.L		A2, -(SP)		ASM_SYS_TRAP (sysTrapStrLen)		MOVE.L		D0, D3		ADDQ.L		#4, SP		JSR			GetConsole		MOVE.W		D3, -(SP)		MOVE.L		A2, -(SP)		MOVE.L		A0, -(SP)		ASM_SYS_TRAP (sysTrapFldInsert)		ADDA.L		#10, SP				MOVEM.L		(SP)+, A0-A2/A5/D0-D3		RTS}		/* Function: WriteNum()   --------------------   Writes the number stored in D0 to the console.  Works by converting the number   to a string (using the trap) and then calling WriteString.  A buffer of 16   characters is allocated for the string.  This should be plenty considering   that the longest possible number represented by 32 bits is -2147483648 (11    characters plus null termination).      Does not trash any registers.  In order to accomplish this, we must save A0-A1   and D0-D2, since these are caller-saved.  A5 is restored from the saved copy, so   that must also be saved.      Known bugs:   - There appears to be a bug with the Palm libraries which makes it fail when     it is supposed to convert -2147483648.  As far as I can tell, all other     numbers work just fine.      Input:     D0.L - Input number.   Internal:     D0.L - Input number initially.     D3.L - Gets input number.     A0.L - Holds buffer pointer temporarily between StrIToA and WriteString. */asm WriteNum(){#define buffer		16		LINK		A6, #-buffer		MOVEM.L		A0-A1/A5/D0-D3, -(SP)		MOVE.L		D0, D3			MOVE.L		D3, -(SP)		PEA			-buffer(A6)		ASM_SYS_TRAP (sysTrapStrIToA)		ADDQ.L		#8, SP		JSR 		WriteString		MOVEM.L		(SP)+, A0-A1/A5/D0-D3		UNLK		A6		RTS}/* Function: NewLine()   -------------------   Moves to the next line.  This is equivalent to writing a '\n' character with   WriteChar() (and in fact, that's how it's implemented).      Internal:     D0.B - Holds a '\n' for the call to WriteChar() */asm NewLine(){		MOVE.B		D0, -(SP)				MOVE.B		#'\n', D0		JSR			WriteChar				MOVE.B		(SP)+, D0		RTS}/* Function: ClearScreen()   -----------------------   Clears the screen.  Basically, just deletes everything in the field.      Internal:     D0.W - Holds the length of the field temporarily.     A0.L - Holds the console for just a second (see A2).     A2.L - Holds the console for the program. */asm ClearScreen(){		MOVEM.L		A0-A2/A5/D0-D2, -(SP)			JSR			GetConsole		MOVEA.L		A0, A2			// Console goes into A2				MOVE.L		A2, -(SP)		ASM_SYS_TRAP (sysTrapFldGetTextLength)		ADDQ.L		#4, SP		MOVE.W		D0, -(SP)		CLR.W		-(SP)		MOVE.L		A2, -(SP)		ASM_SYS_TRAP (sysTrapFldDelete)		ADDQ.L		#8, SP				MOVEM.L		(SP)+, A0-A2/A5/D0-D2		RTS		}/* Function:ReadChar()   -------------------   Reads a character and returns it in the lowest byte of D0.  The high bytes of D0   are set to 0.  The character is not passed onto the system and is not printed to   the console.  Note: currently I cheat and call a C routine to actually get the   character.  That's because I have to go into a tight event loop to wait for the   character and I didn't really want to code that in assembly.  Maybe later.      I clear the high bits by using AND.L.      Does not trash any registers.  In order to accomplish this, we must save A0-A1   and D0-D2, since these are caller-saved.  A5 is restored from the saved copy, so   that must also be saved.      Output:     D0.L - The low byte of D0 gets the character.  The upper bytes are cleared. */asm ReadChar(){		MOVEM.L		A0-A1/A5/D1-D2, -(SP)				JSR 		GetNextChar		AND.L		#0x000000FF, D0				MOVEM.L		(SP)+, A0-A1/A5/D1-D2		RTS}/* Function: MouseClick()   ----------------------   Waits for a mouse click and returns when it gets it.  Very simple form of flow   control.  The mouse click is not passed onto the system.  Note: like for ReadChar(),   I cheat here and call a C routine.      Does not trash any registers.  In order to accomplish this, we must save A0-A1   and D0-D2, since these are caller-saved.  A5 is restored from the saved copy, so   that must also be saved. */asm MouseClick(){		MOVEM.L		A0-A1/A5/D0-D2, -(SP)				JSR 		WaitNextMouse				MOVEM.L		(SP)+, A0-A1/A5/D0-D2		RTS}/* Function: Beep()   ----------------   Does a simple little beep.      Does not trash any registers.  In order to accomplish this, we must save A0-A1   and D0-D2, since these are caller-saved.  A5 is restored from the saved copy, so   that must also be saved. */asm Beep(){		MOVEM.L		A0-A1/A5/D0-D2, -(SP)				MOVE.B		#sndWarning, -(SP)		ASM_SYS_TRAP (sysTrapSndPlaySystemSound)		ADDQ.L		#2, SP			// Need to add _2_ because 68K keeps SP word aligned.				MOVEM.L		(SP)+, A0-A1/A5/D0-D2		RTS}// ----------------------------------// Local functions...// ----------------------------------/* Function: GetConsole()   ----------------------   Gets a pointer to the console and stores it in A0.  The console is the   main field in the form (ID 1001).  Defined in lab6Rsc.h; see line with   field id 1001 - MainDescriptionField.      Trashes registers D0-D2, A0-A1 (called functions do this).      Output:     A0.L - A FieldPtr for the console.   Internal:     D0.L - Scratch: holds return values.     A0.L - Scratch during the function.  Eventually holds the output (a field pointer)     A2.L - Holds the form pointer during the life of the function. */static asm GetConsole(){		MOVE.L		A2, -(SP)				ASM_SYS_TRAP (sysTrapFrmGetActiveForm)		MOVEA.L		A0, A2		MOVE.W 		#MainDescriptionField, -(SP)		MOVE.L		A2, -(SP)		ASM_SYS_TRAP (sysTrapFrmGetObjectIndex)		ADDQ.L		#6, SP				MOVE.W		D0, -(SP)		MOVE.L		A2, -(SP)		ASM_SYS_TRAP (sysTrapFrmGetObjectPtr)		ADDQ.L		#6, SP		MOVEA.L		(SP)+, A2		RTS}	/* Function: GetNextChar()   -----------------------   Gets the next character that the user inputs.  This runs a tight event loop   waiting for a keyDownEvent with a visible character (this allows me to avoid   getting things like the calculator character).      Note that I have to special case the appStopEvent and use a longjmp() to get   back out.  That's what happens when you try to force a sequential paradigm into   an event-driven framework.      @return The character entered. */   static char GetNextChar(){	EventType event;	while (1) {		EvtGetEvent(&event, evtWaitForever);				if (event.eType == keyDownEvent) {			const int kMinVisible = 32;			const int kMaxVisible = 255;			const UInt16 c = event.data.keyDown.chr;						if ((c >= kMinVisible && c <= kMaxVisible) || c == '\n') {				return (char) c;			}		}		SysHandleEvent(&event);				if (event.eType == appStopEvent) {			EvtAddEventToQueue (&event);			ErrLongJump (gExitJmpbuf, 1);		}	}}/* Function: WaitNextMouse()   -------------------------   Like GetNextChar() except a bit simpler.  We don't have to worry about getting   visible characters and all that stuff because we just wait for any mouse up   event and return then.  MouseUp was used instead of MouseDown because otherwise   you don't get a full click.      Note that I have to special case the appStopEvent and use a longjmp() to get   back out.  That's what happens when you try to force a sequential paradigm into   an event-driven framework. */   static void WaitNextMouse(){	EventType event;	while (1) {		EvtGetEvent(&event, evtWaitForever);				if (event.eType == penUpEvent) {			return;		}		SysHandleEvent(&event);		if (event.eType == appStopEvent) {			EvtAddEventToQueue (&event);			ErrLongJump (gExitJmpbuf, 1);		}	}}